FROM rosimd/php-stage

COPY src/ /usr/src/myapp/
WORKDIR /usr/src/myapp

COPY install-composer.sh /install-composer.sh
RUN cd /usr/src/myapp && bash /install-composer.sh
